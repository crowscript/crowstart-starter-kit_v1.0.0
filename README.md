# Project Title 
---
One Paragraph of project description goes here.

## Table of Contents
---
- [About the Project](#about-the-project)
- [Status](#status)
- [Screenshots](#screenshots)
- [Live Demo](#live-demo)
- [Directory Structure](#directory-structure)
- [Getting Started](#getting-started)
- [Features](#features)
	- [Biggest Features 1](#biggest-features-1)
	- [Biggest Features 2](#biggest-features-2)
	- [Additional Features](#additional-features)
- [Built With](#built-with)
	- [How to build](#how-to-build)
	- [Requirements](#requirements)
		- [How install Requirements](#how-install-requirements)
	- [Dependencies](#dependencies)
		- [How install Dependencies](#how-install-the-dependencies)
- [How to use](#how-to-use)
	- [Installing](#installing)
	- [API Reference](#api-reference)
	- [Running the tests](#running-the-tests)
	- [Deployment](#deployment)
- [Contribute](#contribute)
	- [Reporting Bugs](#reporting-bugs)
- [Release Process](#release-process)
	- [Versioning](#versioning)
	- [Changelog](#changelog)
- [Authors](#authors)
- [License](#license)
- [Acknowledgments](#acknowledgments)

## About the Project
---
Here you can provide more details about the project
- What is your project?
- What is the intended use of your project?
- How does it work?
- Who uses it?
- How can others benefit from using your project?
- What features does your project provide?
- Short motivation for the project? (Don't be too long winded)
- Links to the project site

```
Show some example code to describe what your project does
Show some of your APIs
```

## Status
---
![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/atlassian/adf-builder-javascript.svg) ![npm](https://img.shields.io/npm/v/npm.svg) ![node](https://img.shields.io/node/v/passport.svg) ![GitHub release](https://img.shields.io/github/release/qubyte/rubidium.svg) ![PHP version from PHP-Eye](https://img.shields.io/php-eye/symfony/symfony.svg) ![Packagist](https://img.shields.io/packagist/l/doctrine/orm.svg)
[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com) [![forthebadge](http://forthebadge.com/images/badges/uses-css.svg)](http://forthebadge.com) [![forthebadge](http://forthebadge.com/images/badges/powered-by-netflix.svg)](http://forthebadge.com)

Describe the current release and any notes about the current state of the project. Examples: currently compiles on your host machine, but is not cross-compiling for ARM, APIs are not set, feature not implemented, etc.

[Make own badge](https://shields.io/#your-badge)

## Screenshots
---
Include logo/demo screenshot etc.

![Logo](https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/HTML5_logo_resized.svg/170px-HTML5_logo_resized.svg.png)
![Screen](https://lh3.googleusercontent.com/oQ5DNeAAoo3DNecibTFqZXjzVbr_2TU_QOJ7-tIx9RByCcAafggfmREzAjjL1_djQjRGdLJusQ=w640-h400-e365)

## Live Demo
---
A live demo is always the best way to show off your plugin. You can link off to a snapshot or live demo hosted on your Grafana.

[sites.google.com](hhttps://sites.google.com/view/crowscript)

## Directory Structure
---
```
┌── build
│   └── .gitkeep
├── src
│   ├── assets
│   │   ├── css
│   │   │   ├── bootstrap
│   │   │   │   ├── bootstrap.min.css
│   │   │   │   └── bootstrap-theme.min.css
│   │   │   ├── fonts
│   │   │   │   ├── FontAwesome
│   │   │   │   └── ionicons
│   │   │   ├── font-awesome.min.css
│   │   │   └── owl.carousel.css
│   │   ├── img
│   │   │   └── ...
│   │   └── js
│   │       ├── bootstrap
│   │       │   ├── FontAwesome
│   │       │   └── bootstrap.min.js
│   │       ├── jquery.appear.js
│   │       └── smooth.scroll.min.js
│   ├── sass
│   │   ├── _animations.scss
│   │   ├── _core.scss
│   │   ├── _magnificpopup.scss
│   │   ├── _main.scss
│   │   ├── _mixins.scss
│   │   ├── _navigation.scss
│   │   ├── _owl.scss
│   │   ├── _variables.scss
│   │   └── style.scss
│   ├── views
│   │   ├── .gitkeep
│   │   ├── index.html
│   │   └── index.pug
│   ├── favicon.ico
│   ├── humans.txt
│   ├── robots.txt
│   └── sitemap.xml
├── .gitignore
├── CONTRIBUTING.md
├── gulpfile.js
├── LICENSE
├── package.json
├── package-lock.json
└── README.md
```

## Getting Started
---
This section should provide instructions for other developers to

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

- If needed, [install](http://blog.nodeknockout.com/post/65463770933/how-to-install-node-js-and-npm) `node` and `npm` (Node Package Manager).
- If needed, install `gulp` with `npm install gulp -g`.
- Clone this repo with `git clone https://github.com/minamarkham/sassy-starter` or download the zip.
- In terminal, `cd` to the folder containing your project. Alternatively, you can type `cd ` and drag the location of the folder into your terminal and hit enter (on Macs).
- In terminal, type `npm install`. If (and _only_ if) `npm install` isn't working, try `sudo npm install`. This should install all [dependencies](#dependencies).
- In terminal, enter `gulp`.
- Your browser should open at `http://localhost:3000`. You can access this same page on any device on the same wifi network and they'll see whats on your screen. It'll even sync scrolls and clicks!
- Edit your code inside of the `src` folder.
- Your complied and minified css, html, and javascript files will be created and updated in `dist/`. Never edit files within the `dist/` folder, as it gets deleted frequently.
- Keep `gulp` running while you're making changes. When you want to stop the gulp task, hit `ctrl + C`.

_For theming: add separate file (theme.scss) in`src/scss/themes/`, override the default `$theme` variable, and run `gulp themes`._

## Features
---
What makes your project stand out?

### Biggest Features 1
The larger features can demand their own section.

`You can add code blocks.`

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

![featurs1](https://grafana.com/assets/img/blog/mixed_styles.png)

### Biggest Features 2
The larger features can demand their own section.
	
### Additional Features
For non-headline features, bulleted lists can be effective and concise way to increase skimmability.

- Live reloading with BrowserSync
- Image Minification
- Github Pages deployment
- Sass linting (based on [default](https://github.com/sasstools/sass-lint/blob/master/lib/config/sass-lint.yml) config)
- Autoprefixer configuration
- SMACSS and Atomic Design-based folder structure
- `px` to `em`, `px` to `rem` and other useful functions.
- Mixins for inlining media queries.
- Useful CSS helper classes.
- Default print styles, performance optimized.
- "Delete-key friendly." Easy to strip out parts you don't need.
- Includes:
  - [`Normalize.css`](https://necolas.github.com/normalize.css/)
    for CSS normalizations and common bug fixes
  - [`jQuery`](https://jquery.com/) via CDN, with a local fallback
  - [`Modernizr`](http://modernizr.com/), via CDN, for feature
    detection
		
	
## Built With
---
- [html5 boilerplate](https://html5boilerplate.com/)
- [jQuery](https://jquery.com/)
- [Modernizr](https://modernizr.com/)
- [Normalize](https://necolas.github.io/normalize.css/)
- [Pug](https://pugjs.org/api/getting-started.html)

### How to Build
How to build your project

### Requirements
What things you need to install the software and how to install them

- Node/NPM
- LibSass
- Gulp
- Pug - high performance template engine

#### How install Requirements
How to install the requirements.

`npm install pug-cli -g`

**tl;dr**: [Download CrowStart](https://github.com/google/web-starter-kit/releases/latest) and run `$ npm install --global gulp && npm install` in that directory to get started.

-

To take advantage of CrowStart Web Starter Kit you need to:

1. Get a copy of the code.
2. Install the dependencies if you don't already have them.
3. Modify the application to your liking.
4. Deploy your production code.

#### Getting the code

[Download](https://github.com/google/web-starter-kit/releases/latest) and extract WSK to where you want to work.

### Dependencies
Dependencies that need to be installed for building/using your project
```
  "browser-sync": "^2.0.0-rc4",
  "colors": "^1.1.2",
  "del": "^2.0.2",
  "gulp-autoprefixer": "^2.1.0",
  "gulp-concat": "^2.4.3",
  "gulp-gh-pages": "^0.4.0",
  "gulp-imagemin": "^2.1.0",
  "gulp-jshint": "^1.9.0",
  "gulp-minify-css": "^0.3.12",
  "gulp-minify-html": "^0.1.8"
```
#### How install Dependencies
Instructions for installing the dependencies

`
// gulp del
// https://www.npmjs.com/package/del
// npm install --save del

// gulp-newer
// https://www.npmjs.com/package/gulp-newer
// npm install gulp-newer --save-dev

// gulp-imagemin
// https://www.npmjs.com/package/gulp-imagemin
// npm install --save-dev gulp-imagemin

// gulp-sass
// https://www.npmjs.com/package/gulp-sass
// npm install gulp-sass --save-dev

//npm install --save-dev gulp-uglify

// npm install --save-dev gulp-uglify
// https://www.npmjs.com/package/gulp-uglify

//https://www.npmjs.com/package/gulp-concat
// npm install --save-dev gulp-concat

//gulp-connect
// https://www.npmjs.com/package/gulp-connect
// npm install --save-dev gulp-connect

// live reload
/// https://www.npmjs.com/package/gulp-livereload
// npm install --save-dev gulp-livereload

// npm install --save-dev gulp-autoprefixer

// Minify css https://www.npmjs.com/package/gulp-clean-css
// .pipe(cleanCSS())
// npm install gulp-clean-css --save-dev

// gulp-uncss
// https://www.npmjs.com/package/gulp-uncss
// npm install gulp-uncss --save-dev

// PUG
// npm install --save-dev gulp-pug
// https://www.npmjs.com/package/gulp-pug
// https://www.npmjs.com/package/gulp-pug/v/0.3.2

//Remove plugin
// npm uninstall <package-name> --save-dev

// npm install gulp-newer gulp-imagemin gulp-sass del gulp-uglify gulp-concat gulp-connect gulp-livereload gulp-autoprefixer gulp-clean-css gulp-uncss gulp-pug --save-dev
`
## How to Use
---
If people like your project they’ll want to learn how they can use it. To do so include step by step guide to use your project.

### VIEWS

By default, folder `views` provides two folders:

- html
- pug

**PUG** folder provide pug files, and folder includes.
This pug pages files will be export to build folder as HTML pages

**HTML** folder provide HTML files. 
it wont to be export, if you want to work with HTML files, not with pug, please in Gulfile.js setup it.
Find block of code `copy all static HTML to build folder` and uncomment it

#### index.html
This is the default HTML skeleton that should form the basis of all pages on
your site. If you are using a server-side templating framework, then you will
need to integrate this starting HTML with your setup.

Make sure that you update the URLs for the referenced CSS and JavaScript if you
modify the directory structure at all.


### Web App Manifest
HTML5 Boilerplate includes a simple web app manifest file. 

The web app manifest is a simple JSON file that allows you to control how your 
app appears on a device's home screen, what it looks like when it launches
in that context and what happens when it is launched. This allows for much greater
control over the UI of a saved site or web app on a mobile device. 

It's linked to from the HTML as follows:

```html
        <link rel="manifest" href="site.webmanifest">
```
Our [site.webmanifest](https://github.com/h5bp/html5-boilerplate/blob/master/src/site.webmanifest) contains a very skeletal "app" definition, just to show the basic usage. 
You should fill this file out with [more information about your site or application](https://developer.mozilla.org/en-US/docs/Web/Manifest)

### Modernizr

HTML5 Boilerplate uses a custom build of Modernizr.

[Modernizr](https://modernizr.com/) is a JavaScript library which adds classes to
the `html` element based on the results of feature test and which ensures that
all browsers can make use of HTML5 elements (as it includes the HTML5 Shiv).
This allows you to target parts of your CSS and JavaScript based on the
features supported by a browser.

Starting with version 3 Modernizr can be customized using the [modernizr-config.json](https://github.com/h5bp/html5-boilerplate/blob/master/modernizr-config.json) and the
[Modernizr command line utility](https://www.npmjs.com/package/modernizr-cli). 


#### Google Universal Analytics Tracking Code

Finally, an optimized version of the Google Universal Analytics tracking code is
included. Google recommends that this script be placed at the top of the page.
Factors to consider: if you place this script at the top of the page, you’ll
be able to count users who don’t fully load the page, and you’ll incur the max
number of simultaneous connections of the browser.

Further information:

* [Optimizing the Google Universal Analytics
  Snippet](https://mathiasbynens.be/notes/async-analytics-snippet#universal-analytics)
* [Introduction to
  Analytics.js](https://developers.google.com/analytics/devguides/collection/analyticsjs/)
* [Google Analytics Demos & Tools](https://ga-dev-tools.appspot.com/)

**N.B.** The Google Universal Analytics snippet is included by default mainly
because Google Analytics is [currently one of the most popular tracking
solutions](https://trends.builtwith.com/analytics/Google-Analytics) out there.
However, its usage isn't set in stone, and you SHOULD consider exploring the
[alternatives](https://en.wikipedia.org/wiki/List_of_web_analytics_software)
and use whatever suits your needs best!

### vendor

This directory can be used to contain all 3rd party library code.

Minified versions of the latest jQuery and Modernizr libraries are included by
default.

### .htaccess
The default web server configs are for Apache. For more information, please
refer to the [Apache Server Configs
repository](https://github.com/h5bp/server-configs-apache).

### 404.html
A helpful custom 404 to get you started.

### Icons

Replace the default `favicon.ico` and Apple
Touch Icon with your own.

If you want to use different Apple Touch Icons for different resolutions please
refer to the [according documentation](extend.md#apple-touch-icons).

The shortcut icons should be put in the root directory of your site. `favicon.ico` 
is automatically picked up by browsers if it's placed in the root.  HTML5
Boilerplate comes with a default set of icons (include favicon and one Apple
Touch Icon) that you can use as a baseline to create your own.

Please refer to the more detailed description in the [Extend section](extend.md)
of these docs.

### .gitignore

`.gitignore` should
primarily be used to avoid certain project-level files and directories from
being kept under source control. Different development-environments will
benefit from different collections of ignores.

For example, add the following to your `~/.gitconfig`, where the `.gitignore`
in your HOME directory contains the files and directories you'd like to
globally ignore:

```gitignore
[core]
    excludesfile = ~/.gitignore
```

- More on global ignores: https://help.github.com/articles/ignoring-files/
- Comprehensive set of ignores on GitHub: https://github.com/github/gitignore

### .gitkeep
This is file to keep empty folder in Git.

### robots.txt
The `robots.txt` file is used to give instructions to web robots on what can
be crawled from the website.
Edit this file to include any pages you need hidden from search engines.

By default, the file provided by this project includes the next two lines:

 * `User-agent: *` -  the following rules apply to all web robots
 * `Disallow:` - everything on the website is allowed to be crawled

If you want to disallow certain pages you will need to specify the path in a
`Disallow` directive (e.g.: `Disallow: /path`) or, if you want to disallow
crawling of all content, use `Disallow: /`.

The `/robots.txt` file is not intended for access control, so don't try to
use it as such. Think of it as a "No Entry" sign, rather than a locked door.
URLs disallowed by the `robots.txt` file might still be indexed without being
crawled, and the content from within the `robots.txt` file can be viewed by
anyone, potentially disclosing the location of your private content! So, if
you want to block access to private content, use proper authentication instead.

For more information about `robots.txt`, please see:

  * [robotstxt.org](http://www.robotstxt.org/)
  * [How Google handles the `robots.txt` file](https://developers.google.com/webmasters/control-crawl-index/docs/robots_txt)

### humans.txt
The `humans.txt` file is used to provide information about people involved with
the website.
Edit this file to include the team that worked on your site/app, and the
technology powering it.

The provided file contains three sections:

  * `TEAM` - this is intented to list the group of people responsible for the website
  * `THANKS` - this is intended to list the group of people that have contributed
  to the webste
  * `TECHNOLOGY COLOPHON` - the section lists technologies used to make the website
  
For more information about `humans.txt`, please see: http://humanstxt.org/


### browserconfig.xml
This file contains all settings regarding custom tiles for IE11 and Edge.
For more info on this topic, please refer to
[MSDN](https://msdn.microsoft.com/library/dn455106.aspx).
The `browserconfig.xml` file is used to customize the tile displayed when users
pin your site to the Windows 8.1 start screen. In there you can define custom
tile colors, custom images or even [live tiles](https://msdn.microsoft.com/library/dn455106.aspx#CreatingLiveTiles).

By default, the file points to 2 placeholder tile images:

* `tile.png` (558x558px): used for `Small`, `Medium` and `Large` tiles.
  This image resizes automatically when necessary.
* `tile-wide.png` (558x270px): user for `Wide` tiles.

Notice that IE11 uses the same images when adding a site to the `favorites`.

For more in-depth information about the `browserconfig.xml` file, please
see [MSDN](https://msdn.microsoft.com/library/dn320426.aspx).



## Contribute
---
Let people know how they can contribute into your project. A [contributing guideline](https://github.com/zulip/zulip-electron/blob/master/CONTRIBUTING.md) will be a big plus.
Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

### Reporting Bugs
Please list all bugs and feature requests in the Github issue tracker.
This section guides you through submitting a bug report for Atom. Following these guidelines helps maintainers and the community understand your report, reproduce the behavior, and find related reports.

Before creating bug reports, please check this list as you might find out that you don't need to create one. When you are creating a bug report, please include as many details as possible. Fill out the required template, the information it asks for helps us resolve issues faster.

## Release Process
---
Talk about the release process. How are releases made? What cadence? How to get new releases?

### Versioning
This project uses [Semantic Versioning](http://semver.org/). For a list of available versions, see the [repository tag list](https://github.com/your/project/tags).

### Changelog
The README can also include a trailing list of changes for recent versions.

**v1.0.1**
	- Updated the knickerbocker widget to accept multi-input threading.
	- Dialed up the down.
	- Righted the left
**v1.0.0**
	- Fixed bugs introduced in 1.0.4.
	- Introduced bugs that will be fixed in 1.0.6

## Authors
---
* **Stanislav Vranic** - *Developing* - [@crowscript](http://crowscript.com)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License
---
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
[Choose the License](https://choosealicense.com/)

## Acknowledgments
---
This toolkit is based on the work of the following fine people & projects.

- [HTML5 Boilerplate](https://github.com/h5bp/html5-boilerplate)
- [Gulp](https://gulpjs.com/)
- [Sass](https://sass-lang.com/)
- [Scalable and Modular Architecture for CSS](http://smacss.com/book) (<abbr title="Scalable and Modular Architecture for CSS">SMACSS</abbr>)
- [Atomic Design](http://atomicdesign.bradfrost.com)
- [Udacity](https://classroom.udacity.com/courses/ud777)
- [Mina Markham](https://github.com/minamarkham)
- [medium.com/@meakaakka](https://medium.com/@meakaakka/a-beginners-guide-to-writing-a-kickass-readme-7ac01da88ab3)
- [PurpleBooth](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
- [Choose a License](https://choosealicense.com/)
- [Embedded Artistry](https://embeddedartistry.com/blog/2017/11/20/your-readme-probably-sucks-its-time-to-make-it-better)

Provide proper credits, shoutouts, and honorable mentions here. Also provide links to relevant repositories, blog posts, or contributors worth mentioning.

Give proper credits. This could be a link to any repo which inspired you to build this project, any blogposts or links to people who contributed in this project. If you used external code, link to the original source.

**[Back to top](#table-of-contents)**


